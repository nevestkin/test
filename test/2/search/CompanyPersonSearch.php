<?php

namespace medicine\models\search;

use common\models\queries\ActiveQuery;
use medicine\models\CompanyPerson;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * CompanyPersonSearch represents the model behind the search form about `common\models\CompanyPerson`.
 */
class CompanyPersonSearch extends CompanyPerson
{
    /** @var ActiveQuery */
    public $query;
    public $perPage = 20;

    public $company;
    public $person;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['company', 'person'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = $this->query;
        if (!$query) {
            $query = static::find();
        }
        $query->joinWith(['person']);
        $query->joinWith(['company']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $this->perPage,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        // фильтруем по id
        $query->andFilterWhere(['id' => $this->id]);

        // фильтруем по компании
        $query->andFilterWhere([
            'or',
            ['companyId' => $this->company],
            ['like', 'company.name', $this->company],
        ]);

        // фильтруем по персоне
        $query->andFilterWhere([
            'or',
            ['personId' => $this->person],
            ['like', 'person.name', $this->person],
        ]);

        return $dataProvider;
    }
}