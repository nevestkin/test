<?php

namespace medicine\models\search;

use common\models\queries\ActiveQuery;
use medicine\models\Company;
use medicine\models\CompanyPerson;
use medicine\models\Person;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * CompanySearch represents the model behind the search form of `common\models\Company`.
 */
class CompanySearch extends Company
{
    public $person;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'active', 'sortOrder'], 'integer'],
            [['name', 'slug', 'person'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Company::find();
        $query->joinWith(['persons']);
        $query->orderBy('sortOrder');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        // фильтруем по id, slug
        $query->andFilterWhere(['id' => $this->id])
            ->andFilterWhere(['slug' => $this->slug]);

        // фильтруем по персоне
        if (!empty($this->person)) {
            $query->andWhere([
                'and',
                [
                    'or',
                    ['persons.id' => $this->person],
                    ['like', 'persons.name', $this->person],
                ],
                ['active' => Person::IS_ACTIVE]
            ]);
        }

        // фильтруем по name
        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}